/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

/********* Version *********/
/* TalonSRX: 3.9
 * VictorSPX: 3.9
 * Phoenix Framework: 5.6.0 
 */

package org.usfirst.frc.team4060.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.buttons.JoystickButton;


public class Robot extends IterativeRobot {
	/** Hardware */
	TalonSRX _leftBottomMaster = new TalonSRX(13);
		TalonSRX _leftBottomSlave = new TalonSRX(17);
	
	TalonSRX _leftTopMaster = new TalonSRX(11);
		TalonSRX _leftTopSlave = new TalonSRX(15);
	
	TalonSRX _rightBottomMaster = new TalonSRX(12);
		TalonSRX _rightBottomSlave = new TalonSRX(16);
		
	TalonSRX _rightTopMaster = new TalonSRX(10);
		TalonSRX _rightTopSlave = new TalonSRX(14);
	Joystick _gamepad = new Joystick(0);
	
	
	

	@Override
	public void robotInit() {
		/* Don't use this for now */
		PowerDistributionPanel pdu = new PowerDistributionPanel(0);

	
	}
	
	@Override
	public void teleopInit(){
		/* Disable motor controllers */
		_rightTopMaster.set(ControlMode.PercentOutput, 0);
		_leftTopMaster.set(ControlMode.PercentOutput, 0);
		_rightBottomMaster.set(ControlMode.PercentOutput, 0);
		_leftBottomMaster.set(ControlMode.PercentOutput, 0);
		
		
		
			_rightTopSlave.set(ControlMode.PercentOutput, 0);
			_leftTopSlave.set(ControlMode.PercentOutput, 0);
			_rightBottomSlave.set(ControlMode.PercentOutput, 0);
			_leftBottomSlave.set(ControlMode.PercentOutput, 0);
			
		
		/* Set Neutral mode */
		_leftTopMaster.setNeutralMode(NeutralMode.Brake);
		_rightTopMaster.setNeutralMode(NeutralMode.Brake);
		_leftBottomMaster.setNeutralMode(NeutralMode.Brake);
		_rightBottomMaster.setNeutralMode(NeutralMode.Brake);
			
			_leftTopSlave.setNeutralMode(NeutralMode.Brake);
			_rightTopSlave.setNeutralMode(NeutralMode.Brake);
			_leftBottomSlave.setNeutralMode(NeutralMode.Brake);
			_rightBottomSlave.setNeutralMode(NeutralMode.Brake);
		
		/* Configure output direction */
		_leftTopMaster.setInverted(false);
		_rightTopMaster.setInverted(true);
		_leftBottomMaster.setInverted(false);
		_rightBottomMaster.setInverted(true);
		
			_leftTopSlave.setInverted(false);
			_rightTopSlave.setInverted(true);
			_leftBottomSlave.setInverted(false);
			_rightBottomSlave.setInverted(true);
			
		
			
			
				//	JoystickButton mouthOpen = new JoystickButton(_gamepad, 1);
			
			
			
			
		
		
		
		System.out.println("This is a basic arcade drive using Arbitrary Feed Forward.");
	}
	
	@Override
	public void teleopPeriodic() {		
		/* Gamepad processing */
		double forward = 1 * _gamepad.getY();
		double rotate = 1 * _gamepad.getZ();
		double turn = _gamepad.getTwist();		
		forward = Deadband(forward);
		turn = Deadband(turn);

		/* Basic Arcade Drive using PercentOutput along with Arbitrary FeedForward supplied by turn */
		
		if (forward != 0 && rotate == 0) {
		_leftTopMaster.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
		_rightTopMaster.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
		_leftBottomMaster.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
		_rightBottomMaster.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
		
		_leftTopSlave.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
		_rightTopSlave.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
		_leftBottomSlave.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, +turn);
		_rightBottomSlave.set(ControlMode.PercentOutput, forward, DemandType.ArbitraryFeedForward, -turn);
		
		}
		
		else if (rotate != 0 && forward == 0) {
			_leftTopMaster.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
			_rightTopMaster.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
			_leftBottomMaster.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
			_rightBottomMaster.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
			
			_leftTopSlave.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
			_rightTopSlave.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
			_leftBottomSlave.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, turn);
			_rightBottomSlave.set(ControlMode.PercentOutput, rotate, DemandType.ArbitraryFeedForward, -turn);
		}
		}
	
		
		
		
		//System.out.print(_leftTopMaster.getOutputCurrent());
		
	
		
		/*
		_leftTopMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightTopMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		_leftBottomMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightBottomMaster.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		
		_leftTopSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightTopSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		_leftBottomSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, +turn);
		_rightBottomSlave.set(ControlMode.Velocity, rotate+forward, DemandType.ArbitraryFeedForward, -turn);
		
		
		//if (mouthOpen == 1) {}
			
		
		*/
		
	

	/** Deadband 5 percent, used on the gamepad */
	double Deadband(double value) {
		/* Upper deadband */
		if (value >= +0.75) 
			return value;
		
		/* Lower deadband */
		if (value <= 0.75)
			return value;
		
		/* Outside deadband */
		return 0;
	}
}
